use metrics::TorvusMetrics;
use prometheus::Registry;
use prometheus_hyper::Server as PrometheusServer;
use serenity::{
    async_trait,
    model::{
        channel::Message,
        event::ResumedEvent,
        gateway::{Activity, Ready},
        id::{ChannelId, GuildId},
        user::OnlineStatus,
    },
    prelude::*,
    utils::Colour,
};
use std::{env, net::SocketAddr, sync::Arc, thread, time::Duration};
use tokio::runtime::Runtime;
use tokio::sync::Notify;
use veloren_client::{addr::ConnectionArgs, Client as VelorenClient, Event as VelorenEvent};
use veloren_common::{clock::Clock, comp, util::DISPLAY_VERSION_LONG};
use serenity::model::prelude::UserId;
use std::str::FromStr;

mod metrics;

const TPS: u64 = 60;

enum VelorenMessage {
    Connect,
    Disconnect,
    Chat(String, Colour),
    Players(String, String),
}

enum DiscordMessage {
    Chat { nickname: String, msg: Message },
}

struct Handler {
    messages_rx: async_channel::Receiver<VelorenMessage>,
    server_addr: ConnectionArgs,
    guild: u64,
    bridge_channel: u64,
    whitelisted_bots: Vec<UserId>,
    discord_msgs_tx: async_channel::Sender<DiscordMessage>,
    torvus_metrics: Arc<TorvusMetrics>,
    _metrics_shutdown: Arc<Notify>,
}

async fn connect_to_veloren(
    addr: ConnectionArgs,
    torvus_metrics_clone: &Arc<TorvusMetrics>,
    messages_tx: &async_channel::Sender<VelorenMessage>,
    veloren_username: &str,
    veloren_password: &str,
    trusted_auth_server: &str,
    runtime: Arc<Runtime>,
) -> veloren_client::Client {
    let mut retry_cnt = [0u32; 3];
    'connect: loop {
        log::debug!("Connecting...");
        let mut veloren_client =
            match VelorenClient::new(addr.clone(), None, Arc::clone(&runtime)).await {
                Ok(client) => client,
                Err(e) => {
                    log::error!(
                        "Failed to connect to Veloren server: {:?}, retry: {}",
                        e,
                        retry_cnt[0]
                    );
                    retry_cnt[0] += 1;
                    torvus_metrics_clone.veloren_connected_error.inc();
                    tokio::time::sleep(Duration::from_millis(500) * retry_cnt[0]).await;
                    continue 'connect;
                }
            };
        retry_cnt[0] = 0;

        if let Err(e) = veloren_client
            .register(
                veloren_username.into(),
                veloren_password.into(),
                |auth_server| auth_server == trusted_auth_server,
            )
            .await
        {
            log::error!(
                "Failed to switch to registered state: {:?}, retry: {}",
                e,
                retry_cnt[1]
            );
            retry_cnt[1] += 1;
            torvus_metrics_clone.veloren_register_error.inc();
            tokio::time::sleep(Duration::from_secs(30) * retry_cnt[1]).await;
            continue 'connect;
        }
        retry_cnt[1] = 0;

        messages_tx.try_send(VelorenMessage::Connect).unwrap();

        log::debug!("Logged in.");

        veloren_client.send_chat("/alias Discord_Bridge".to_string());

        return veloren_client;
    }
}

#[allow(clippy::too_many_arguments)]
impl Handler {
    pub fn new(
        addr: ConnectionArgs,
        guild: u64,
        bridge_channel: u64,
        whitelisted_bots: Vec<UserId>,
        veloren_username: String,
        veloren_password: String,
        trusted_auth_server: String,
        torvus_metrics: Arc<TorvusMetrics>,
        metrics_shutdown: Arc<Notify>,
        runtime: Arc<Runtime>,
    ) -> Self {
        let (messages_tx, messages_rx) = async_channel::bounded(100);
        let (discord_msgs_tx, discord_msgs_rx) = async_channel::bounded(100);

        let torvus_metrics_clone = Arc::clone(&torvus_metrics);
        let addr_clone = addr.clone();
        tokio::task::spawn_blocking(move || {
            let mut retry_cnt = [0u32; 3];

            'connect: loop {
                let mut veloren_client = runtime.block_on(connect_to_veloren(
                    addr.clone(),
                    &torvus_metrics_clone,
                    &messages_tx,
                    &veloren_username,
                    &veloren_password,
                    &trusted_auth_server,
                    Arc::clone(&runtime),
                ));

                let mut clock = Clock::new(Duration::from_secs_f64(1.0 / TPS as f64));

                loop {
                    let events = match veloren_client.tick(
                        comp::ControllerInputs::default(),
                        clock.dt(),
                        |_| {},
                    ) {
                        Ok(events) => events,
                        Err(e) => {
                            log::error!("Failed to tick client: {:?}, retry: {}", e, retry_cnt[2]);
                            retry_cnt[2] += 1;
                            thread::sleep(Duration::from_secs(10) * retry_cnt[2]);
                            messages_tx.try_send(VelorenMessage::Disconnect).unwrap();
                            continue 'connect;
                        }
                    };
                    retry_cnt[2] = 0;

                    if let Ok(DiscordMessage::Chat { nickname, msg }) = discord_msgs_rx.try_recv() {
                        let msg: Message = msg;
                        let message = deunicode::deunicode(&msg.content).to_string();
                        if !message.starts_with('/') {
                            veloren_client.send_chat(format!("{}: {}", nickname, message));
                        } else {
                            match message.as_str() {
                                "/players" => veloren_client.send_chat(message),
                                _ => log::error!("Intercepted message: {}", message),
                            }
                        }
                        torvus_metrics_clone.messages_to_veloren.inc();
                    }

                    for event in events {
                        match event {
                            VelorenEvent::Chat(msg) => {
                                torvus_metrics_clone.messages_from_veloren.inc();
                                let message = veloren_client.format_message(&msg, true)
                                    // Remove all escape characters before further sanitizing
                                    .replace(r"\", "")
                                    // Escape all the formatting characters
                                    .replace('*', r"\*")
                                    .replace('_', r"\_")
                                    .replace('`', r"\`")
                                    .replace('~', r"\~")
                                    // Prevent sending formatted URLs and mentions with zero-width spaces
                                    .replace('(', "\u{200B}(")
                                    .replace('<', "<\u{200B}");

                                if message.contains(" online players:") {
                                    if let Some(index) = message.find('\n') {
                                        let (title, players) = message.split_at(index);
                                        messages_tx
                                            .try_send(VelorenMessage::Players(
                                                title.to_string(),
                                                players
                                                    .to_string()
                                                    .replace('[', "**[")
                                                    .replace(']', "]** "),
                                            ))
                                            .unwrap();
                                    }
                                } else {
                                    use veloren_common::comp::chat::ChatType;
                                    let colour = match msg.chat_type {
                                        ChatType::Online(_) => Colour::TEAL,
                                        ChatType::Offline(_) => Colour::PURPLE,
                                        ChatType::Kill(_, _) => Colour::RED,
                                        ChatType::World(_) => Colour::BLURPLE,
                                        _ => Colour::GOLD,
                                    };

                                    let message = match msg.chat_type {
                                        ChatType::Online(_) | ChatType::Offline(_) => message
                                            .find(']')
                                            .map(|index| {
                                                // It's safe to increment index by 1 here because the size of the ']' character is always a single UTF-8 codepoint.
                                                let (user, message) = message.split_at(index + 1);
                                                format!("***{}*** *{}*", user, message.trim())
                                            })
                                            .unwrap_or(message),
                                        ChatType::World(uid) => message
                                            .find(':')
                                            .map(|index| {
                                                let (user, message) = message.split_at(index);
                                                // The URL contains the player's character name instead of just a placeholder.
                                                // Why? I just think it's more fun this way c:
                                                let char_name = veloren_client
                                                    .player_list()
                                                    .get(&uid)
                                                    .map(|player_info| {
                                                        player_info
                                                            .character
                                                            .clone()
                                                            .map(|char_info| {
                                                                let name = char_info.name.trim();
                                                                name.chars()
                                                                    // This is so you don't get %XX codes in the URL which make the name less readable
                                                                    .map(|c| {
                                                                        if c.is_whitespace() {
                                                                            '_'
                                                                        } else if !c.is_ascii() {
                                                                            '+'
                                                                        } else {
                                                                            c
                                                                        }
                                                                    })
                                                                    .collect::<String>()
                                                                    + ".character_name"
                                                            })
                                                            .unwrap_or("no_character".to_owned())
                                                    })
                                                    .unwrap_or("player_does_not_exist".to_owned());

                                                // Alternative char_name, to only use it for highlighting the username:
                                                //let char_name = "this-is-here-just-to-highlight-the-username";

                                                format!(
                                                    // URLs ending in .invalid are guaranteed to not be registered
                                                    // according to IETF's RFC 2606 https://tools.ietf.org/html/rfc2606
                                                    "**[{}](https://{}.invalid){}**",
                                                    user,
                                                    char_name,
                                                    message.trim_end()
                                                )
                                            })
                                            .unwrap_or(message),
                                        ChatType::Kill(_, _) => format!(
                                            "*{}*",
                                            message
                                                .replace('[', "**[")
                                                .replace(']', "]**")
                                                .trim()
                                                // The following is needed because PC and mobile Discord versions
                                                // have inconsistencies between their markdown parsers
                                                .replace("]** ", "]*** *")
                                                .replace(" **[", "* ***[")
                                        ),
                                        _ => format!("*{}*", message.trim()),
                                    };

                                    messages_tx
                                        .try_send(VelorenMessage::Chat(message, colour))
                                        .unwrap();
                                }
                            }
                            VelorenEvent::Disconnect => {
                                torvus_metrics_clone.messages_from_veloren.inc();
                                messages_tx.try_send(VelorenMessage::Disconnect).unwrap();
                            }
                            VelorenEvent::DisconnectionNotification(_) => {
                                log::debug!("Will be disconnected soon! :/")
                            }
                            VelorenEvent::Notification(notification) => {
                                log::debug!("Notification: {:?}", notification);
                            }
                            _ => {}
                        }
                    }
                    veloren_client.cleanup();

                    clock.tick();
                }
            }
        });

        Self {
            messages_rx,
            server_addr: addr_clone,
            guild,
            bridge_channel,
            whitelisted_bots,
            discord_msgs_tx,
            torvus_metrics,
            _metrics_shutdown: metrics_shutdown,
        }
    }
}

#[async_trait]
impl EventHandler for Handler {
    async fn ready(&self, ctx: Context, ready: Ready) {
        log::info!("Connected as {}", ready.user.name);

        ctx.set_presence(Some(Activity::playing("Veloren")), OnlineStatus::Online)
            .await;

        let channel_id = ChannelId(self.bridge_channel);
        let server_addr = self.server_addr.clone();
        let messages_rx = self.messages_rx.clone();

        let messages_to_discord_clone = self.torvus_metrics.messages_to_discord.clone();

        let _handle = tokio::spawn(async move {
            while let Ok(msg) = messages_rx.recv().await {
                let send_msg = |text, colour| {
                    channel_id.send_message(&ctx.http, move |m| {
                        m.embed(|e| e.colour(colour).description(text))
                    })
                };

                let send_players_list = |(title, players)| {
                    channel_id.send_message(&ctx.http, |m| {
                        m.embed(|e| {
                            e.colour(Colour::ORANGE)
                                .title(title)
                                .description(players)
                        })
                    })
                };

                match msg {
                    VelorenMessage::Chat(chat_msg, colour) => {
                        if !chat_msg.starts_with("**[[You]") {
                            log::debug!("[Veloren] {}", chat_msg);
                            if let Err(e) = send_msg(chat_msg, colour).await {
                                log::error!("Failed to send discord message: {}", e);
                            } else {
                                messages_to_discord_clone.inc();
                            }
                        }
                    }
                    VelorenMessage::Players(title, players) => {
                        log::debug!("[Veloren] {} {}", title, players);
                        if let Err(e) = send_players_list((title, players)).await {
                            log::error!("Failed to send discord message: {}", e);
                        } else {
                            messages_to_discord_clone.inc();
                        }
                    }
                    VelorenMessage::Connect => {
                        ctx.online().await;
                        log::info!("Server bridge connected to {:?}.", server_addr);
                    }
                    VelorenMessage::Disconnect => {
                        ctx.invisible().await;
                        log::info!("Server bridge disconnected from {:?}.", server_addr);
                    }
                }
            }
        });
    }

    async fn resume(&self, _: Context, _: ResumedEvent) {
        log::info!("Connection to discord resumed.");
    }

    async fn message(&self, ctx: Context, msg: Message) {
        let guild = GuildId(self.guild);

        if *msg.channel_id.as_u64() != self.bridge_channel {
            return;
        }

        let author = &msg.author;
        if !author.bot || self.whitelisted_bots.contains(&author.id) {
            self.torvus_metrics.messages_from_discord.inc();
            log::debug!("[Discord] {}", &deunicode::deunicode(&msg.content));
            self.discord_msgs_tx
                .send(DiscordMessage::Chat {
                    nickname: msg
                        .author
                        .nick_in(ctx.http, guild)
                        .await
                        .unwrap_or(msg.clone().author.name),
                    msg,
                })
                .await
                .unwrap();
        }
    }
}

fn init_metrics() -> (Arc<TorvusMetrics>, Arc<Notify>) {
    let registry = Arc::new(Registry::new());
    let (torvus_metrics, registry_torvus_metrics) = TorvusMetrics::new().unwrap();
    let torvus_metrics_arc = Arc::new(torvus_metrics);
    registry_torvus_metrics(&registry).expect("failed to register torvus metrics");

    let metrics_server: SocketAddr = env::var("METRICS_SERVER")
        .expect("No environment variable 'METRICS_SERVER' found.")
        .parse()
        .expect("Invalid address!");

    let metrics_shutdown = Arc::new(Notify::new());
    let metrics_shutdown_clone = metrics_shutdown.clone();

    tokio::spawn(async move {
        PrometheusServer::run(
            Arc::clone(&registry),
            metrics_server,
            metrics_shutdown_clone.notified(),
        )
        .await
    });

    (torvus_metrics_arc, metrics_shutdown)
}

fn main() {
    let runtime = Arc::new(
        tokio::runtime::Builder::new_multi_thread()
            .enable_all()
            .build()
            .unwrap(),
    );
    let runtime2 = Arc::clone(&runtime);

    runtime.block_on(async {
        kankyo::init().ok();
        env_logger::init();

        let token: String = env_key("DISCORD_TOKEN");
        let veloren_server = ConnectionArgs::resolve(
            &env::var("VELOREN_SERVER").expect("No environment variable 'VELOREN_SERVER' found."),
            false,
        )
        .await
        .expect("Invalid address/dns resolve failed");
        let guild = env_key("DISCORD_GUILD");
        let bridge_channel = env_key("DISCORD_CHANNEL");
        let whitelisted_bots = env_key::<String>("DISCORD_BOT_WHITELIST")
            .split_whitespace()
            .map(|user_id| UserId::from_str(user_id).unwrap())
            .collect();

        let veloren_username = env_key("VELOREN_USERNAME");
        let veloren_password = env_key("VELOREN_PASSWORD");
        let trusted_auth_server = env_key("VELOREN_TRUSTED_AUTH_SERVER");

        let (torvus_metrics, metrics_shutdown) = init_metrics();

        let handler = Handler::new(
            veloren_server,
            guild,
            bridge_channel,
            whitelisted_bots,
            veloren_username,
            veloren_password,
            trusted_auth_server,
            torvus_metrics,
            metrics_shutdown,
            runtime2,
        );

        log::info!("Veloren-Common/Client version: {}", *DISPLAY_VERSION_LONG);

        let mut client = Client::builder(&token)
            .event_handler(handler)
            .await
            .expect("Failed to create serenity client!");

        client.start().await.expect("Failed to start client.");
    });
}

fn env_key<T>(key: &str) -> T
where
    T: std::str::FromStr,
    <T as std::str::FromStr>::Err: std::fmt::Debug,
{
    env::var(key)
        .unwrap_or_else(|_| panic!("No environment variable '{}' found.", key))
        .parse()
        .unwrap_or_else(|_| panic!("'{}' couldn't be parsed.", key.to_string()))
}
